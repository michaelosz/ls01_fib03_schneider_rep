import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

class Fahrkartenautomat {

    private static final DecimalFormat df = new DecimalFormat("0.00");
    private static final Scanner tastatur = new Scanner(System.in);
    private static java.text.NumberFormat format;
    public static HashMap<Integer, Ticket> tickets = new HashMap<>();

    public static void main(String[] args) {
        java.util.Currency eur = java.util.Currency.getInstance("EUR");
        new Ticket(1, "Einzelfahrschein Regeltarif AB", 2.9);
        new Ticket(2, "Tageskarte Regeltarif AB ", 8.6);
        new Ticket(3, "Kleingruppen-Tageskarte Regeltarif AB", 23.5);
        format = java.text.NumberFormat.getCurrencyInstance(java.util.Locale.GERMANY);
        format.setCurrency(eur);
        df.setRoundingMode(RoundingMode.UP);
        verkaufStarten();
    }

    private static void verkaufStarten() {
        System.out.println("Fahrkartenbestellvorgang:\n" +
                "=========================\n");
        double zuZahlenderBetrag = fahrkartenbestellungErfassen();
        double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        fahrkartenAusgeben();
        rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
    }

    private static double fahrkartenbestellungErfassen() {
        System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
        for (Map.Entry<Integer, Ticket> integerTicketEntry : tickets.entrySet()) {
            System.out.println("\t" + integerTicketEntry.getValue().getName() + " [" + format.format(integerTicketEntry.getValue().getPrice()) + " EUR] (" + integerTicketEntry.getKey() + ")");
        }
        System.out.println();

        int eingabeWahl;
        while (true) {
            System.out.print("Ihre Wahl: ");
            eingabeWahl = tastatur.nextInt();
            if (tickets.containsKey(eingabeWahl)) {
                break;
            }
            System.out.println(">>falsche Eingabe<<");
        }

        System.out.print("Wieviel Fahrkarten möchten Sie von " + tickets.get(eingabeWahl).getName() + " haben? ");
        double fahrkartenAnzahl = tastatur.nextInt();

        return tickets.get(eingabeWahl).getPrice() * fahrkartenAnzahl;
    }

    private static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        // Geldeinwurf
        // -----------
        double eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.println("Noch zu zahlen: " + format.format(zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            double eingeworfeneMuenze = tastatur.nextDouble();
            if(eingeworfeneMuenze >= 500d){
                System.out.println("Nix da, zu hohe Summe!\nWir akzeptieren keine 500er Scheine...");
                continue;
            }
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
        return eingezahlterGesamtbetrag;
    }

    private static void fahrkartenAusgeben() {
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            warte(250);
        }
        System.out.println("\n\n");
    }

    private static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;

        if (rueckgabebetrag > 0.0) {
            System.out.println("Der Rückgabebetrag in Höhe von " + format.format(rueckgabebetrag));
            System.out.println("wird in folgenden Münzen ausgezahlt:");
            while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                muenzeAusgeben(2, "EURO");
                rueckgabebetrag -= 2.0;
                rueckgabebetrag = round(rueckgabebetrag);
            }
            while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                muenzeAusgeben(1, "EURO");
                rueckgabebetrag -= 1.0;
                rueckgabebetrag = round(rueckgabebetrag);
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                muenzeAusgeben(50, "CENT");
                rueckgabebetrag -= 0.5;
                rueckgabebetrag = round(rueckgabebetrag);
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                muenzeAusgeben(20, "CENT");
                rueckgabebetrag -= 0.2;
                rueckgabebetrag = round(rueckgabebetrag);
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                muenzeAusgeben(10, "CENT");
                rueckgabebetrag -= 0.1;
                rueckgabebetrag = round(rueckgabebetrag);
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                muenzeAusgeben(5, "CENT");
                rueckgabebetrag -= 0.05;
                rueckgabebetrag = round(rueckgabebetrag);
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                "vor Fahrtantritt entwerten zu lassen!\n" +
                "Wir wünschen Ihnen eine gute Fahrt.");

        System.out.println("\n\nWiederhole verkauf!\n\n");
        verkaufStarten();
    }

    private static void warte(int millisekunde) {
        try {
            Thread.sleep(millisekunde);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void muenzeAusgeben(int betrag, String einheit) {
        System.out.println(betrag + " " + einheit);
    }

    private static double round(double ammount) {
        return (double) Math.round(ammount * 100) / 100;
    }

}

public class Ticket {

    private String name;
    private double price;
    private int id;

    public Ticket(int id, String name, double price){
        Fahrkartenautomat.tickets.put(id, this);
        this.name = name;
        this.id = id;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }
}
